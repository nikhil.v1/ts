"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
const Three = {
    one() {
        console.log("One");
    },
    two() {
        console.log("two");
    },
};
let age = "10";
const s1 = {
    name: "Nikhil",
    age: 10,
    greet: ((place) => `welcome ${place}`)
};
let a = 10;
let b = 20;
let c = null;
let d = undefined;
let e = NaN;
function sum(a, b) {
    return a + b;
}
function sub(a, b) {
    return a - b;
}
function mul(a, b) {
    return a * b;
}
const result = function (fun1, a, b) {
    return fun1(a, b);
};
let user = [10, "hello"];
user.push(10);
user.push("hii");
function book(name, price, review = false) {
    return `Book name:${name} 
    Review: ${review},
    Price : ${price}`;
}
let user1 = {
    name: "Nikhil",
    isPad: true,
};
function UserCreate(user) {
    return `${user.name} and ${user.isPad}`;
}
let name1 = "Hello Bye ";
function myFunction() { }
let bool = true;
let ans = a + name1;
let ans1 = bool + name1;
let num = 11;
function isEven(num) {
    if (num % 2 == 0) {
        return true;
    }
    return false;
}
let num1 = "Helllo";
num1 = 80;
num1 = "true";
if (typeof num1 === "number") {
}
else if (typeof num1 === "string") {
}
function calculateTax(income) {
    if (income < 50000) {
        return 0;
    }
    else if (income >= 50000 && income <= 100000) {
        return 10;
    }
    return 20;
}
const x = function (num) {
    if (typeof num !== "number") {
        return parseInt(num);
    }
    return num * 1;
};
const user2 = (user) => {
    console.log(`${user.name} pass:${user.passwd}, is there${user.isActive}, follower are ${user.follower}`);
    return user.id;
};
let order1 = {
    id: 1,
    order_name: "Pizza",
    order_price: 500,
    order_method: "upi",
    payment_order: {
        cardName: "Visa",
        cardNumber: 12345,
    },
};
const create_Order = (o) => {
    if (o.order_method == "cash") {
        o.payment_order.cardName = "---";
        o.payment_order.cardNumber = 0o0;
    }
    console.log(`
    Id = ${o.id} 
    Name = ${o.order_name}
    Price =${o.order_price}
    Method to Pay= ${o.order_method}
    Details of method:
                 Name= ${o.payment_order.cardName} and Number= ${o.payment_order.cardNumber} 
     `);
};
order1.order_method = "visa";
order1.payment_order.cardNumber = 95789238;
const createLogin = (lacc) => {
    return `Email : ${lacc.email} and passwd : ${lacc.passwd} remember :${lacc.rememberME}`;
};
let acc1 = {
    email: "xyz@gamil.com",
    passwd: "passwd123",
    rememberME: true,
};
let acc2 = {
    email: "abc@gmail.com",
    passwd: "poiuy"
};
class Fb {
    constructor(email, passwd) {
        this.email = email;
        this.passwd = passwd;
        this.email = email;
        this.passwd = passwd;
    }
    get getPasswd() {
        return this.passwd;
    }
    set setPasswd(pasttpasswd) {
        this.passwd = pasttpasswd;
    }
}
const userFb1 = new Fb("abc@gmail,com", "OO0o0;");
userFb1.setPasswd = "12344560";
class plastic {
    constructor(platics_name, prices) {
        this.platics_name = platics_name;
        this.prices = prices;
    }
}
class plasticD extends plastic {
    constructor(platics_name, prices) {
        super(platics_name, prices);
        this.platics_name = platics_name;
        this.prices = prices;
    }
    getPla() {
        console.log("getPla");
        return 10;
    }
}
const p1 = new plasticD("heloo", 123);
class Ride {
    start() {
        Ride.activeRide++;
    }
    end() {
        Ride.activeRide--;
    }
}
Ride.activeRide = 0;
let rider1 = new Ride();
let rider2 = new Ride();
rider1.start();
rider2.start();
rider2.start();
class alice {
    constructor(name, rank) {
        this.name = name;
        this.rank = rank;
    }
    get getinfo() {
        return `Alice ${this.name} and ${this.rank}`;
    }
}
class Animal extends alice {
    constructor(name, rank) {
        super(name, rank);
    }
    get getinfo() {
        return `Animal :${this.name}  and ${this.rank}`;
    }
}
class aqua extends alice {
    constructor(name, rank) {
        super(name, rank);
    }
    get getinfo() {
        return `Aqua :${this.name}  and ${this.rank}`;
    }
}
const a1 = new alice("human", 1);
const a22 = new aqua("aqua", 2);
function printName(single) {
    for (const data of single) {
        console.log(data.getinfo);
    }
}
let employee = {
    name: "John Smith",
    salary: 50000,
    address: { street: "Flinders st", city: "Melbourne", zipCode: 3144 },
};
class numKeyValue {
    constructor(key, value) {
        this.key = key;
        this.value = value;
    }
}
let a2 = new numKeyValue(1, "Nikhil");
let a3 = new numKeyValue("2", 40000000);
function wrap(value) {
    return value;
}
function echo(value) {
    console.log(value);
}
function Compnent(constructor) {
    console.log("Component decotor  called");
    constructor.prototype.uniqueId = Date.now();
    constructor.prototype.insertInDOM = () => {
        console.log("Inserting the component in the DOM");
    };
}
function ABC(constructor) {
    console.log("Hii");
}
let ProfileComponent = class ProfileComponent {
};
ProfileComponent = __decorate([
    ABC
], ProfileComponent);
