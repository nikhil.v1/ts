//____   Intersection -------
type One={
   one:()=> void
   
}


type Two ={
  two:()=>void
}

type three=One&Two

const Three :three={
  one() {
    console.log("One")
  },
  two() {
    console.log("two")
  },
}

// Three.one()
// Three.two()

// Union 

let age : number | string="10"
//age=20
//console.log(age)

//call signature

type Student={
  name:string,
  age:number,
  greet:(place:string)=>string // here the call signature is implement as the greet function given the anatotion is there not the  working 
}

const s1:Student={
  name:"Nikhil",
  age:10,
  greet:((place):string => `welcome ${place}`) 
}

//console.log(s1.greet("India"))


let a: number = 10;
let b: number = 20;

let c = null; //obj
let d = undefined; //undefined
let e = NaN; //number

//console.log(typeof c)

function sum(a: number, b: number) {
  return a + b;
}
function sub(a: number, b: number) {
  return a - b;
}
function mul(a: number, b: number):number {
  return a * b;
}

const result = function (fun1: any, a: number, b: number) {
  return fun1(a, b);
};
//console.log(result(sum,e,b))

let user: [number, string] = [10, "hello"];
//console.log(user)
user.push(10);
// user.push(false)
user.push("hii")
//console.log(user)

function book(name: String, price: number, review: boolean = false) {
  return `Book name:${name} 
    Review: ${review},
    Price : ${price}`;
}

//console.log(book("One",600))
//console.log(book("Two",600,true))

//function

let user1 = {
  name: "Nikhil",
  isPad: true,
};
function UserCreate(user:any) {
  return `${user.name} and ${user.isPad}`;
}
//console.log(usercreate(user1))

let name1: string = "Hello Bye ";

//console.log(name1.replace(/ /g,"_"))

function myFunction() {}
//console.log(myFunction.length); // 0
let bool: boolean = true;

let ans: string = a + name1; // 10Hello Bye

let ans1 = bool + name1;// trueHello Bye
//console.log(ans1)

let num = 11;

function isEven(num: number) {
  if (num % 2 == 0) {
    return true;
  }
  return false;
}

//console.log(isEven(num))

let num1: unknown = "Helllo";

num1 = 80;
num1 = "true";

if (typeof num1 === "number") {
  //    console.log("Number")
} else if (typeof num1 === "string") {
  //  console.log("String")
}

//console.log(calculateTax(40_000));

function calculateTax(income: number): number {
  if (income < 50_000) {
    return 0;
  } else if (income >= 50_000 && income <= 1_00_000) {
    return 10;
  }
  return 20;
}

const x = function (num: number | string): number {
  if (typeof num !== "number") {
    return parseInt(num);
  }
  return num * 1;
};
//console.log(x("4"))

// ---------------------------------type aliases----------------------------------------------

type User = {
  id: number;
  name: string;
  passwd: string;
  isActive: boolean;
  follower: number;
};

const user2 = (user: User): number => {
  console.log(
    `${user.name} pass:${user.passwd}, is there${user.isActive}, follower are ${user.follower}`
  );
  return user.id;
};

//console.log(user2({id:1,name:'lucky',passwd:"qwertyui",isActive:true,follower:100}))


type cardInfo = {
  cardNumber: number;
  cardName: string;
};

type Order = {
  readonly id: number;
  order_name: string;
  order_price: number;
  order_method: PaymentMethod;
  payment_order: cardInfo;
};

type PaymentMethod = "cash" | "visa" | "upi";
let order1: Order = {
  id: 1,
  order_name: "Pizza",
  order_price: 500,
  order_method: "upi",
  payment_order: {
    cardName: "Visa",
    cardNumber: 12345,
  },
};

const create_Order = (o: Order) => {
  if (o.order_method == "cash") {
    o.payment_order.cardName = "---";
    o.payment_order.cardNumber = 0o0;
  }

  console.log(`
    Id = ${o.id} 
    Name = ${o.order_name}
    Price =${o.order_price}
    Method to Pay= ${o.order_method}
    Details of method:
                 Name= ${o.payment_order.cardName} and Number= ${o.payment_order.cardNumber} 
     `);
};

order1.order_method = "visa";
order1.payment_order.cardNumber = 95789238;
//create_Order(order1)

//------------------------------interface--------------------------------------

interface login {
  email: string;
  passwd: string;
}
interface login {
    rememberME: boolean;
  }
const createLogin = (lacc: login): string => {
  return `Email : ${lacc.email} and passwd : ${lacc.passwd} remember :${lacc.rememberME}`;
};

let acc1: login = {
  email: "xyz@gamil.com",
  passwd: "passwd123",
  rememberME: true,
};



//console.log(createLogin(acc1))

//delete the interface entities
type loginreview=Omit<login,"rememberME">

let acc2:loginreview={
    email:"abc@gmail.com",
    passwd:"poiuy"
}



//---------------- --------------class-----------------------------

class Fb {
  constructor(public email: string, private passwd: string) {
    this.email = email;
    this.passwd = passwd;
  }

  get getPasswd(): string {
    return this.passwd;
  }

  set setPasswd(pasttpasswd: string) {
    this.passwd = pasttpasswd;
  }
}

const userFb1 = new Fb("abc@gmail,com", "OO0o0;");

//console.log(userFb1);
//console.log(userFb1.getPasswd);
userFb1.setPasswd = "12344560";
//console.log(userFb1.getPasswd)

//----------------abstract---------------------------
abstract class plastic {
  constructor(public platics_name: string, public prices: number) {}

  abstract getPla(): void;
}
class plasticD extends plastic {
  constructor(override platics_name: string, override prices: number) {
    super(platics_name, prices);
  }

  getPla(): number {
    console.log("getPla");
    return 10;
  }
}
const p1 = new plasticD("heloo", 123);
// console.log(p1)
// console.log(p1.getPla())

//-------------------------------------------------------------------------------

class Ride {
  static activeRide: number = 0;

  start() {
    Ride.activeRide++;
  }
  end() {
    Ride.activeRide--;
  }
}

let rider1 = new Ride();
let rider2 = new Ride();
rider1.start();
rider2.start();
rider2.start();

//console.log(rider1," ",rider2)
//console.log(Ride.activeRide)
//-------------------------------------------------------------------------------
class alice {
  constructor(public name: string, public rank: number) {}

  get getinfo() {
    return `Alice ${this.name} and ${this.rank}`;
  }
}
class Animal extends alice {
  constructor(name: string, rank: number) {
    super(name, rank);
  }
  override get getinfo() {
    return `Animal :${this.name}  and ${this.rank}`;
  }
}

class aqua extends alice {
  constructor(name: string, rank: number) {
    super(name, rank);
  }
  override get getinfo() {
    return `Aqua :${this.name}  and ${this.rank}`;
  }
}
const a1 = new alice("human", 1);
const a22=new aqua("aqua",2)
//  console.log(a1)
//  console.log(a22)
// console.log(a1.getinfo)
// a1.name="Shark"
// console.log(a1.getinfo)

function printName(single: alice[]) {
  for (const data of single) {
    console.log(data.getinfo);
  }
}

// printName([
//     new  aqua("shark",0),
//     new  Animal("being human",2)
// ])

// class logger{
//     constructor (public name  :string){}

//     file_name(){
//      console.log(this.name)
//     }
// }

let employee = {
  name: "John Smith",
  salary: 50_000,
  address: { street: "Flinders st", city: "Melbourne", zipCode: 3144 },
};

interface Details{
    street:string,
    city:string,
    zipCode:Number
}
interface employee{
    name:string,
    salary:45_000,
    address:Details

}
///------------------------------------generic-------------
//generic class
class numKeyValue<K,V>{
    constructor(public key:K,public value:V){}

}

let a2=new numKeyValue<number,string>(1,"Nikhil")
let a3=new numKeyValue<string,number>("2",40_000_000)
//console.log(a2)

function wrap<T>(value:T){
  return value
}
//console.log(wrap<number>(1234))

function  echo<T extends number|string>(value:T){
 console.log(value)
}

//echo("string")

//++++++++++++ Decorate ++++++++++++++
function Compnent(constructor:Function){
  console.log("Component decotor  called")
  constructor.prototype.uniqueId= Date.now()
  constructor.prototype.insertInDOM=()=>{
    console.log("Inserting the component in the DOM")
  }
}

function ABC(constructor:Function){
  console.log("Hii")
}

@ABC
class ProfileComponent{
  
}

